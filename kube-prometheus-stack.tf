locals {
  stack_name = "kube-prometheus-stack"
}
resource "helm_release" "kube_prometheus_stack" {
  name = local.stack_name

  repository       = "https://prometheus-community.github.io/helm-charts"
  chart            = "kube-prometheus-stack"
  version          = var.kube_prometheus_stack_version
  namespace        = local.monitoring_namespace
  create_namespace = true

  # Note: regional kube-prometheus-stack-values.yaml are generated via tenctl+jsonnet
  values = [
    file("${path.module}/r${var.site_index}-kube-prometheus-stack-values.yaml")
  ]

  # Note if you're considering using `set` values here:
  # Unless you have a good reason (eg, passing in a secret or data lookup)
  # Please consider rather applying changes to the values in this
  # helm chart through the `kube-stack-chart-mixins` mechanism
  # This avoids having two ways to configure a single configuration
  # which in turn reduces cognitive load, and makes testing easier.

  set {
    name  = "grafana.adminPassword"
    value = var.grafana_password
  }

  set {
    name  = "alertmanager.config.global.slack_api_url"
    value = var.slack_url
  }

  set {
    name  = "alertmanager.config.receivers[0].pagerduty_configs[0].service_key"
    value = var.pagerduty_service_key
  }

  dynamic "set" {
    for_each = var.kube_prometheus_additional_values
    content {
      name  = set.value.name
      value = set.value.value
    }
  }
}

locals {
  rules_files = yamldecode(file("${path.module}/r${var.site_index}-prometheus-rules-chart-values.yaml"))["rulesFiles"]
  checksums   = { for key, filename in local.rules_files : key => filemd5("../../../${filename}") }
}

resource "helm_release" "prometheus_rules" {
  name = "instrumentor-rules"

  chart     = "${path.module}/rules-chart"
  namespace = local.monitoring_namespace

  values = [
    file("${path.module}/r${var.site_index}-prometheus-rules-chart-values.yaml")
  ]

  set {
    name  = "rule_checksums"
    value = yamlencode(local.checksums)
  }

  depends_on = [helm_release.kube_prometheus_stack]
}
