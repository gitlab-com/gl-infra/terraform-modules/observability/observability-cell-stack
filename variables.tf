variable "kube_prometheus_stack_version" {
  type        = string
  description = "Version (without v-prefix) of kube-prometheus-stack helm chart to deploy"
  nullable    = false
}

variable "site_index" {
  type        = number
  description = "Relative index of this site within the site_regions array in the tenant model."
  nullable    = false
}

variable "grafana_password" {
  type        = string
  description = "Password for Grafana admin account"
  sensitive   = true
  nullable    = false
}

variable "slack_url" {
  type        = string
  description = "Slack URL environment secret for alerting"
  sensitive   = true
  nullable    = false
}

variable "pagerduty_service_key" {
  type        = string
  description = "Pagerduty service key environment secret for alerting"
  sensitive   = true
  nullable    = false
}

variable "kube_prometheus_additional_values" {
  type = list(object({
    name  = string
    value = string
  }))
  description = "Additional set values to pass to the kube-prometheus-stack helm chart"
  nullable    = false
}

variable "redis_address" {
  type        = string
  description = "Address of redis instance"
  nullable    = false
}

variable "redis_password" {
  type        = string
  description = "Password for tenant redis instance"
  sensitive   = true
  nullable    = false
}


variable "load_dashboards" {
  type = list(object({
    path          = string
    files         = set(string)
    fail_on_empty = optional(bool, true)
  }))
  description = "A set of dashboards to load for the tenant"
  nullable    = false
}
