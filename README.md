# `tenant-observability-stack`

This module contains all common, kubernetes deployed observability tools, shared between AWS and GCP tenants.

Please do not add cloud-specific resources into this module.

Please attempt to add all non-cloud specific observability to this module, so that it can be shared between AWS
and GCP tenants.

<!-- BEGIN_TF_DOCS -->


## Resources

| Name | Type |
|------|------|
| [helm_release.cert_exporter](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.kube_prometheus_stack](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.prometheus_rules](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.redis_exporter](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_config_map.dashboards](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/config_map) | resource |
| [kubernetes_secret.redis_password](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_grafana_password"></a> [grafana\_password](#input\_grafana\_password) | Password for Grafana admin account | `string` | n/a | yes |
| <a name="input_kube_prometheus_additional_values"></a> [kube\_prometheus\_additional\_values](#input\_kube\_prometheus\_additional\_values) | Additional set values to pass to the kube-prometheus-stack helm chart | <pre>list(object({<br>    name  = string<br>    value = string<br>  }))</pre> | n/a | yes |
| <a name="input_kube_prometheus_stack_version"></a> [kube\_prometheus\_stack\_version](#input\_kube\_prometheus\_stack\_version) | Version (without v-prefix) of kube-prometheus-stack helm chart to deploy | `string` | n/a | yes |
| <a name="input_load_dashboards"></a> [load\_dashboards](#input\_load\_dashboards) | A set of dashboards to load for the tenant | <pre>list(object({<br>    path          = string<br>    files         = set(string)<br>    fail_on_empty = optional(bool, true)<br>  }))</pre> | n/a | yes |
| <a name="input_pagerduty_service_key"></a> [pagerduty\_service\_key](#input\_pagerduty\_service\_key) | Pagerduty service key environment secret for alerting | `string` | n/a | yes |
| <a name="input_redis_address"></a> [redis\_address](#input\_redis\_address) | Address of redis instance | `string` | n/a | yes |
| <a name="input_redis_password"></a> [redis\_password](#input\_redis\_password) | Password for tenant redis instance | `string` | n/a | yes |
| <a name="input_site_index"></a> [site\_index](#input\_site\_index) | Relative index of this site within the site\_regions array in the tenant model. | `number` | n/a | yes |
| <a name="input_slack_url"></a> [slack\_url](#input\_slack\_url) | Slack URL environment secret for alerting | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_monitoring_namespace"></a> [monitoring\_namespace](#output\_monitoring\_namespace) | namespace for the tenant monitoring stack |
| <a name="output_prometheus_service_url"></a> [prometheus\_service\_url](#output\_prometheus\_service\_url) | Service URL for the Prometheus service |
<!-- END_TF_DOCS -->
