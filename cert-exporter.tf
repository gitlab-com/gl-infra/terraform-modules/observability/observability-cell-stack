resource "helm_release" "cert_exporter" {
  name = "cert-exporter"

  repository = "https://joe-elliott.github.io/cert-exporter"
  chart      = "cert-exporter"
  version    = "3.5.0" # TODO: manage this with Renovate/.gitlab-ci-other-versions.yml
  namespace  = local.monitoring_namespace

  set {
    name  = "certManager.additionalPodLabels.app"
    value = "cert-exporter"
  }

  set_list {
    name  = "certManager.rbac.clusterRole.rules[0].resources"
    value = ["configmaps", "secrets"]
  }

  set {
    name  = "certManager.nodeSelector.workload"
    value = "support"
  }

  set {
    name  = "certManager.dashboards.certManagerDashboard.create"
    value = false
  }

  set_list {
    name  = "certManager.image.args"
    value = ["--secrets-annotation-selector=cert-manager.io/certificate-name", "--secrets-annotation-selector=dedicated.gitlab.com/pages-certificate-name", "--secrets-include-glob=*.crt", "--logtostderr"]
  }

  depends_on = [
    helm_release.kube_prometheus_stack,
  ]
}
