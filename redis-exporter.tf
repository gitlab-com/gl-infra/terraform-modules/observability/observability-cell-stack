resource "kubernetes_secret" "redis_password" {
  metadata {
    name      = "redis-exporter-auth-secret"
    namespace = local.monitoring_namespace
  }

  data = {
    password = var.redis_password
  }

  depends_on = [
    helm_release.kube_prometheus_stack, # for namespace creation
  ]
}

resource "helm_release" "redis_exporter" {
  name = "redis-exporter"

  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-redis-exporter"
  version    = "6.2.0"

  namespace = local.monitoring_namespace

  set {
    name  = "nodeSelector.workload"
    value = "observability"
  }

  set {
    name  = "tolerations[0].key"
    value = "dedicated/workload"
  }

  set {
    name  = "tolerations[0].value"
    value = "observability"
  }

  set {
    name  = "tolerations[0].operator"
    value = "Equal"
  }

  set {
    name  = "tolerations[0].effect"
    value = "NoSchedule"
  }

  set {
    name  = "redisAddress"
    value = var.redis_address
  }

  set {
    name  = "auth.enabled"
    value = "true"
  }

  set {
    name  = "auth.secret.name"
    value = kubernetes_secret.redis_password.metadata[0].name
  }

  set {
    name  = "auth.secret.key"
    value = "password"
  }
}
